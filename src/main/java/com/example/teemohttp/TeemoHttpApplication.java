package com.example.teemohttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeemoHttpApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeemoHttpApplication.class, args);
    }

}
